const Discord = require('discord.js')
const config = require('./config.json')
const client = new Discord.Client()

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`)
})

client.on('message', msg => {
    const prefix = config.prefix
    if (!msg.content.startsWith(prefix) || msg.author.bot) return
    const args = msg.content.slice(prefix.length).split(' ')
    const command = args.shift().toLowerCase()


    if (command === 'hug') {
        if ((!(msg.mentions.users.size === 0))) {
            const user = getUserFromMention(args[0])
            msg.channel.send('Hey ' + user + '! ``' + msg.author.username + '`` just sent you a BIG hug! <:dabheart:579061109798207500>')
        } else {
            msg.reply('You need to mention someone in order to hug them!!')
        }
    }
})

function getUserFromMention(mention) {
    // The id is the first and only match found by the RegEx.
    const matches = mention.match(/^<@!?(\d+)>$/)

    // If supplied variable was not a mention, matches will be null instead of an array.
    if (!matches) return

    // However the first element in the matches array will be the entire mention, not just the ID,
    // so use index 1.
    const id = matches[1]

    return client.users.get(id)
}


client.login(config.token)